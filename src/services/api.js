import axios from 'axios';
import url from '../config';

class API {
  getInTime() {
    return axios.get(url.apiURL);
  }
}

export default new API();