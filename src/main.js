import Vue from 'vue'
import App from './App.vue'
import API from './services/api';


Vue.config.productionTip = false
Vue.prototype.apiService = API;

new Vue({
  render: h => h(App),
}).$mount('#app')
